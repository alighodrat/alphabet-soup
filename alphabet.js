const fs = require('fs');

function loadWordSearch(file) {
    const data = fs.readFileSync(file, 'utf8');
    const lines = data.split('\r\n').map(line=>line.split([' ']).join(''));
    const [size, ...gridAndWords] = lines;
    const [rows, cols] = size.split('x');
    const grid = gridAndWords.slice(0, rows);
    const words = gridAndWords.slice(rows);

    const firstCharsMap = {};
    grid.forEach((line,lineIndex)=> {
        for (let charIndex = 0; charIndex < line.length; charIndex++) {
            const ch = line[charIndex];
            if(firstCharsMap[ch])
                firstCharsMap[ch].push({lineIndex, charIndex});
            else
                firstCharsMap[ch] = [{lineIndex, charIndex}];
        }
    })

    return { rows: parseInt(rows), cols: parseInt(cols), grid, words, firstCharsMap };
}

function findWordsAndPrint(wordSearch) {
    const { rows, cols, grid, words, firstCharsMap } = wordSearch;

    const directions = [
        [0, 1],   // horizontal (right)
        [0, -1],  // horizontal (left)
        [1, 0],   // vertical (down)
        [-1, 0],  // vertical (up)
        [1, 1],   // diagonal (down-right)
        [-1, -1], // diagonal (up-left)
        [1, -1],  // diagonal (down-left)
        [-1, 1],  // diagonal (up-right)
    ];

    function isValidIndex(row, col) {
        return row >= 0 && row < rows && col >= 0 && col < cols;
    }

    function searchWord(word, row, col, dirRow, dirCol) {
        const wordLength = word.length;
        const lastRow = row + (wordLength - 1) * dirRow;
        const lastCol = col + (wordLength - 1) * dirCol;

        if (!isValidIndex(lastRow, lastCol)) {
            return false;
        }

        for (let i = 0; i < wordLength; i++) {
            const currentRow = row + i * dirRow;
            const currentCol = col + i * dirCol;
            const currentChar = grid[currentRow][currentCol];

            if (currentChar !== word[i]) {
                return false;
            }
        }

        return true;
    }

    function findWord(word) {
        const wordLength = word.length;

        if(firstCharsMap[word[0]]){
            firstCharsMap[word[0]].forEach(xy=> {
                const row = xy.lineIndex;
                const col = xy.charIndex;
                for (const [dirRow, dirCol] of directions) {
                    if (searchWord(word, row, col, dirRow, dirCol)) {
                        const startRow = row;
                        const startCol = col;
                        const endRow = row + (wordLength - 1) * dirRow;
                        const endCol = col + (wordLength - 1) * dirCol;
                        console.log(`${word} ${startRow}:${startCol} ${endRow}:${endCol}`);
                        break;
                    }
                }
            })
        }
    }

    words.map(findWord);
}


const wordSearch = loadWordSearch('input.txt');
findWordsAndPrint(wordSearch);

